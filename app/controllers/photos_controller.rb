class PhotosController < ApplicationController
	before_action :authenticate_user!, :only => [:create]

	def create
		@place = Place.find(params[:place_id])
		@place.photos.create(photos_params)
		redirect_to place_path(@place)
	end

	def destroy
		@place = Place.find(params[:place_id])
		@photo = Photo.find(params[:id])

		if @photo.destroy
			redirect_to place_path(@place), notice: 'photo has been deleted'
		else
			redirect_to place_path(@place), notice: 'error deleting'
		end
	end

	private

	def photos_params
		params.require(:photo).permit(:picture, :caption)
	end
end
